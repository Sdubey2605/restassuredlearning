package RestAssuredLearnByAmod.RAPractice;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class GetRequest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		//Build request 
		RestAssured.baseURI="https://restful-booker.herokuapp.com/";
		RestAssured.basePath="booking/{id}";
		
		RequestSpecification reqspe=RestAssured.given();
		reqspe.pathParam("id", 9);
		//hit tyhe request 
		Response res=reqspe.get();
		//validate response 
		ValidatableResponse validres=res.then().log().all();
		validres.statusCode(200);
		*/
		
		//less code
		
		RestAssured.given()
		
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/{id}")
		.pathParam("id", 1)
		.get()
		.then()
		.log()
		.all()
		.statusCode(200);
	

	}

}
